using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPVdoParcial
{
    public partial class Crear : Form
    {
        string localI;
        string localB;
        string port;
        string address;
        string password;
        string redundancia;
        string options;

        public Crear()
        {
            InitializeComponent();
            cbxRedundancia.Items.Add("High");
            cbxRedundancia.Items.Add("Normal");
            cbxRedundancia.Items.Add("Low");

            cbxOption.Text = "Full";
            cbxOption.Items.Add("Part");
            cbxOption.Items.Add("Min");
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Form1 formBase = new Form1();
            formBase.Show();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            leerDatos();
            listCArchivo.Items.Add("LOCALIDINVENTARIO: " + localI);
            listCArchivo.Items.Add("LOCALIDADBASE: " + localB);
            listCArchivo.Items.Add("install.port: " + port);
            listCArchivo.Items.Add("install.address: " + address);
            listCArchivo.Items.Add("install.password: " + password);
            listCArchivo.Items.Add("install.redundancia: " + redundancia);
            listCArchivo.Items.Add("install.option: " + options);
        }

        private void leerDatos()
        {
            if(localI == "")
            {
                MessageBox.Show("FAVOR DE LLENAR LOS CAMPOS VACIOS");
            }
            else if(localB == "")
            {
                MessageBox.Show("FAVOR DE LLENAR LOS CAMPOS VACIOS");
            }
            else if(port == "")
            {
                MessageBox.Show("FAVOR DE LLENAR LOS CAMPOS VACIOS");
            }
            else if(address == "")
            {
                MessageBox.Show("FAVOR DE LLENAR LOS CAMPOS VACIOS");
            }
            else if(password == "")
            {
                MessageBox.Show("FAVOR DE LLENAR LOS CAMPOS VACIOS");
            }
            else if(redundancia == "")
            {
                MessageBox.Show("FAVOR DE LLENAR LOS CAMPOS VACIOS");
            }
            else if(options == "")
            {
                MessageBox.Show("FAVOR DE LLENAR LOS CAMPOS VACIOS");
            }
            else
            {
                localI = txtLInventario.Text;
                localB = txtLBase.Text;
                port = txtPort.Text;
                address = txtAddress.Text;
                password = txtPasword.Text;
                redundancia = cbxRedundancia.Text;
                options = cbxOption.Text;

                MessageBox.Show("SE HAN GUARDADO CON EXITO LOS DATOS");
            }
        }

        private void btnArchivo_Click(object sender, EventArgs e)
        {
            TextWriter archivo;
            archivo = new StreamWriter("examen.txt");
            archivo.WriteLine("LOCALIDADINVENTARIO: " + localI);
            archivo.WriteLine("LOCALIDADBASE: " + localB);
            archivo.WriteLine("install.port: " + port);
            archivo.WriteLine("install.address: " + address);
            archivo.WriteLine("install.password: " + password);
            archivo.WriteLine("install.redundancia: " + redundancia);
            archivo.WriteLine("install.option: " + options);
            archivo.Close();

            MessageBox.Show("TU ARCHIVO TXT HA SIDO CREADO");
        }
    }
}
