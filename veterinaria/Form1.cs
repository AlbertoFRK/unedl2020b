using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace veterinaria
{
    public partial class Form1 : Form
    {
        List<mascotas> lstMascotas = new List<mascotas>();

        public Form1()
        {
            InitializeComponent();
            btnBuscar.Enabled = false;
            btnEliminar.Enabled = false;
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if(ValidarRaza() == false)
            {
                return;
            }
            if (ValidarNombreM() == false)
            {
                return;
            }
            if (ValidarNombreD() == false)
            {
                return;
            }
            if (ValidarNumero() == false)
            {
                return;
            }

            mascotas myMascota = new mascotas();
            myMascota.Raza = txtNumero.Text;
            myMascota.NombreM = txtNMascota.Text;
            myMascota.NombreD = txtNDueño.Text;
            myMascota.Numero = int.Parse(txtNumero.Text);
            lstMascotas.Add(myMascota);
            listBox1.DataSource = null;
            listBox1.DataSource = lstMascotas;
            LimpiarControles();
            txtRaza.Focus();
            btnBuscar.Enabled = true;
        }

        //Validaciones
        private bool ValidarRaza()
        {
            if(string.IsNullOrEmpty(txtRaza.Text))
            {
                errorProvider1.SetError(txtRaza, "ingrese la raza");
                return false;
            }
            else
            {
                errorProvider1.SetError(txtRaza, "");
                return true;
            }
        }

        private bool ValidarNombreM()
        {
            if (string.IsNullOrEmpty(txtNMascota.Text))
            {
                errorProvider1.SetError(txtNMascota, "ingrese el nombre de la mascota");
                return false;
            }
            else
            {
                errorProvider1.SetError(txtNMascota, "");
                return true;
            }
        }

        private bool ValidarNombreD()
        {
            if (string.IsNullOrEmpty(txtNDueño.Text))
            {
                errorProvider1.SetError(txtNDueño, "ingrese el nombre del dueño");
                return false;
            }
            else
            {
                errorProvider1.SetError(txtNDueño, "");
                return true;
            }
        }

        private bool ValidarNumero()
        {
            int Num;
            if (!int.TryParse(txtNumero.Text, out Num) || txtNumero.Text == "")
            {
                errorProvider1.SetError(txtNumero, "ingrese el numero telefonico");
                txtNumero.Clear();
                txtNumero.Focus();
                return false;
            }
            else
            {
                errorProvider1.SetError(txtNumero, "");
                return true;
            }
        }

        //Metodo para limpiar los controles
        private void LimpiarControles()
        {
            txtRaza.Clear();
            txtNMascota.Clear();
            txtNDueño.Clear();
            txtNumero.Clear();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if(ValidarNombreM() == false)
            {
                return;
            }
            mascotas myBMascota = GetMascota(txtNMascota.Text); 
            if(myBMascota == null)
            {
                errorProvider1.SetError(txtNMascota, "La mascota no esta registrada");
                LimpiarControles();
                txtNMascota.Focus();
                return;
            }
            else
            {
                errorProvider1.SetError(txtNMascota, "");
                txtRaza.Text = myBMascota.Raza;
                txtNMascota.Text = myBMascota.NombreM;
                txtNDueño.Text = myBMascota.NombreD;
                txtNumero.Text = myBMascota.Numero.ToString();
                btnEliminar.Enabled = true;
            }
        }

        private mascotas GetMascota(string nombre)
        {
            return lstMascotas.Find(mascota => mascota.NombreM.Contains(nombre));
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (txtNMascota.Text == "")
            {
                errorProvider1.SetError(txtNMascota, "Indique el nombre de la mascota para eliminarla");
                LimpiarControles();
                txtNMascota.Focus();
                btnEliminar.Enabled = false;
                return;
            }
            else
            {
                errorProvider1.SetError(txtNMascota, "");
                DialogResult Respuesta = MessageBox.Show("¿Esta seguro de eliminar este registro?",
                "Confimacion", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if(Respuesta == DialogResult.Yes)
                {
                    foreach (mascotas myMascota in lstMascotas)
                    {
                        if(myMascota.NombreM == txtNMascota.Text)
                        {
                            lstMascotas.Remove(myMascota);
                            break;
                        }
                    }
                    LimpiarControles();
                    listBox1.DataSource = null;
                    listBox1.DataSource = lstMascotas;
                }
            }
        }
    }
}
