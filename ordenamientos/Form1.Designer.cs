
namespace ordenaminetos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnQS = new System.Windows.Forms.Button();
            this.btnBS = new System.Windows.Forms.Button();
            this.btnMS = new System.Windows.Forms.Button();
            this.btnHS = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(109, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(268, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ordenaminetos";
            // 
            // btnQS
            // 
            this.btnQS.Location = new System.Drawing.Point(129, 247);
            this.btnQS.Name = "btnQS";
            this.btnQS.Size = new System.Drawing.Size(106, 54);
            this.btnQS.TabIndex = 1;
            this.btnQS.Text = "QuickSort";
            this.btnQS.UseVisualStyleBackColor = true;
            this.btnQS.Click += new System.EventHandler(this.btnQS_Click);
            // 
            // btnBS
            // 
            this.btnBS.Location = new System.Drawing.Point(255, 247);
            this.btnBS.Name = "btnBS";
            this.btnBS.Size = new System.Drawing.Size(106, 53);
            this.btnBS.TabIndex = 2;
            this.btnBS.Text = "BubbleSort";
            this.btnBS.UseVisualStyleBackColor = true;
            this.btnBS.Click += new System.EventHandler(this.btnBS_Click);
            // 
            // btnMS
            // 
            this.btnMS.Location = new System.Drawing.Point(129, 307);
            this.btnMS.Name = "btnMS";
            this.btnMS.Size = new System.Drawing.Size(106, 53);
            this.btnMS.TabIndex = 4;
            this.btnMS.Text = "MergeSort";
            this.btnMS.UseVisualStyleBackColor = true;
            this.btnMS.Click += new System.EventHandler(this.btnMS_Click);
            // 
            // btnHS
            // 
            this.btnHS.Location = new System.Drawing.Point(255, 307);
            this.btnHS.Name = "btnHS";
            this.btnHS.Size = new System.Drawing.Size(105, 53);
            this.btnHS.TabIndex = 5;
            this.btnHS.Text = "HeapSort";
            this.btnHS.UseVisualStyleBackColor = true;
            this.btnHS.Click += new System.EventHandler(this.btnHS_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(167, 79);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(146, 20);
            this.textBox1.TabIndex = 6;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(167, 143);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(146, 20);
            this.textBox2.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 388);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnHS);
            this.Controls.Add(this.btnMS);
            this.Controls.Add(this.btnBS);
            this.Controls.Add(this.btnQS);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQS;
        private System.Windows.Forms.Button btnBS;
        private System.Windows.Forms.Button btnMS;
        private System.Windows.Forms.Button btnHS;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
    }
}

